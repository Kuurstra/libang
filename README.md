# libang

Library for angular measurements related to fieldmaps.  Includes phase unwrapping and frequency estimation algorithms.

examples:
```python
# given a complex array "data" and echo times "tes"

# method 1: estimate frequency using the first three echoes and a second difference 
freq_2nd_diff = freq_est.second_difference(np.angle(data), tes)

# The following are two other methods to estimating frequency using 
# weighted least squares on the unwrapped phase.

# optional: remove first echo if measurements have phase singularities or offsets
data_phase = np.angle(data * data[..., :1].conj())[..., 1:]
tes_corrected = tes[1:] - tes[0]

# least squares weights should be proportional to 1/variance.
# phase stdv is proportional to 1/SNR. 
# therefore, phase variance is proportional to 1 / mag^2 and weights = mag^2
weights = np.abs(data[..., 1:])**2

# method 2: temporal uwrap
# use the second difference frequency estimate to unwrap the phase
t_unwrapped = unwrap.temporal_freq_est(data_phase, freq_2nd_diff, tes_corrected)
freq_t_unwrap = freq_est.lwlsq(t_unwrapped, tes_corrected, weight=weights)

# method 3: spatial temporal unwrap
mask = np.ones(data.shape[:-1])
st_unwrapped, reliability = unwrap.spatial_temporal(data_phase, tes_corrected, mask)
freq_st_unwrap = freq_est.lwlsq(st_unwrapped, tes_corrected, weight=weights)
```