# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 16:08:32 2016

@author: Alan
"""

import numpy as np


def second_difference(phase_img, te):
    # 2nd difference estimate of 1st derivative (freq) when non-uniform sampling
    # only uses 1st 3 echoes
    # te = [t1, t2, t3, ..]
    # dt1 = t2-t1
    # dt2 = t3-t2

    # taylor approx
    # f(t - dt1) = f(t) - dt1 f'(t) + dt1^2 f''(t)/2! + ...
    # f(t + dt2) = f(t) + dt2 f'(t) + dt2^2 f''(t)/2! + ...
    # error on 2nd difference estimate of 1st derivative
    # ( f(t + dt2) - f(t) ) - ( f(t) - f(t-dt1) ) = dt2 f'(t) + dt2^2 f''(t)/2! - dt1 f'(t) + dt1^2 f''(t)/2!
    # ( f(t + dt2) - f(t) ) - ( f(t) - f(t-dt1) ) / (dt2 - dt1) = f'(t) + (dt2^2+dt1^2) / (dt2 - dt1) f''(t)/2!
    return np.angle(np.exp(1j * (phase_img[..., 2] - 2 * phase_img[..., 1] + phase_img[..., 0]))) \
           / (te[2] - 2 * te[1] + te[0])

def lwlsq(phase_img, te, weight=None):
    # estimate frequency using linear weighted least squares
    # assumes phase_img is unwrapped
    # echo is last dimension of phase_img

    # WLS
    # y = Ax
    # (A' W)y = A' W A x^
    # x^ = inverse(A' W A) A' W y

    # phase_1_voxel = te * f_1_voxel
    # f_1_voxel^ = inverse(te' W te) te' W phase_1_voxel

    orig_shape = phase_img.shape
    n = np.prod(orig_shape[:-1])
    phase_img = phase_img.reshape((n, orig_shape[-1]))
    if weight is None:
        freqest = 1.0 / np.dot(te, te) * np.dot(phase_img, te)
    else:
        weight = weight.reshape((n, orig_shape[-1]))
        freqest = 1.0 / np.dot(weight * te, te) * np.dot(weight * phase_img, te)
        weight = weight.reshape(orig_shape)

    freqest = freqest.reshape(orig_shape[:-1])

    return freqest


