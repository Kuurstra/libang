import numpy as np
from skimage.restoration import unwrap_phase as spatial_unwrap

from libang.freq_est import lwlsq


def round(array):
    # desired: always round x.5 to lowest magnitude
    # np.floor(dp + 0.5) always rounds 0.5 to highest array
    # np.ceil(dp - 0.5) always rounds 0.5 to lowest array
    # np.round(dp) always rounds 0.5 to the nearest even

    # rounds to highest value
    rounded = np.floor(array + 0.5)
    # correct values where array value was x.5 and x is positive
    rounded[np.logical_and((rounded - array) == 0.5, array > 0)] -= 1
    return rounded


def temporal_1st_diff(phase_img, hz=False, modify_original=False):
    # there can be no phase_img wraps by the first sample, we must trust t1
    # phase_img evolution between t2 and t1 should be less than pi
    # if we see a difference in phase_img of pi or more, then we can assume it's due to a wrap
    # https://numpy.org/doc/stable/reference/generated/numpy.unwrap.html
    # https://www.mathworks.com/help/dsp/ref/unwrap.html#mw_6964e809-11b4-4753-af1a-2b805e83102f
    # https://git.cfmm.uwo.ca/applications/freqmap/-/blob/master/unwrapTemporal.m

    phase_img = np.array(phase_img)
    if modify_original:
        phase_unwrap = phase_img
    else:
        phase_unwrap = phase_img.copy()

    # each sample should be within (-pi, pi] of the previous sample
    # how many 2 pis are needed to get close to the previous sample
    # current = current + round( (prev - current)/(2*pi) ) * (2*pi)
    # note, for Hz data each sample should be within (-0.5, 0.5] of the previous sample

    if hz:
        factor = 1
    else:
        factor = 2 * np.pi

    # np.diff = (current - prev)
    # -np.diff = prev - current
    dp = - np.diff(phase_unwrap, 1, axis=-1)
    dp_corr = round(dp / factor)
    phase_unwrap[..., 1:] += np.cumsum(dp_corr, axis=-1) * factor
    return phase_unwrap


def temporal_2nd_diff(phase_img, hz=False, modify_original=False):
    # there can be no phase_img wraps by t2, we must trust t1 and t2
    # 1) if non-uniform sampling a signal with constant frequency, then we assume the phase_img evolution
    # in (t[n] - t[n-1]) - (t[n-1] - t[n-2]) is less than pi. If there is a large phase_img evolution
    # then we assume a phase_img wrap and correct the phase_img at t[n] to reduce |(p[n] - p[n-1]) - (p[n-1] - p[n-2])|.
    # Eg, consider te = [0.1, 0.2, 0.31]. (p[3]-p[2]) will have evolved more than (p[2]-p[1]) because there
    # was 0.01 more precession time, but it should not have evolved very much

    # 2) if uniform sampling a signal with varying frequency
    # we make the assumption that phase_img acceleration is less than pi.

    phase_img = np.array(phase_img)
    if modify_original:
        phase_unwrap = phase_img
    else:
        phase_unwrap = phase_img.copy()

    if hz:
        factor = 1
    else:
        factor = 2 * np.pi

    # how many 2 pis are needed to get close to the previous 1st difference
    # current_1st_diff = current_1st_diff + round( (prev_1st_diff - current_1st_diff)/(2*pi) ) * (2*pi)
    # p[n] - p[n-1] = p[n] - p[n-1] + round( ((p[n-1] - p[n-2]) - (p[n] - p[n-1]))/(2*pi) ) * (2*pi)
    # p[n] = p[n] + round( (2p[n-1] - p[n-2] - p[n])/(2*pi) ) * (2*pi)

    # np.diff2 = (p[n] - p[n-1]) - (p[n-1] - p[n-2]) = p[n] - 2p[n-1] + p[n-2]
    # -np.diff2 = (2p[n-1] - p[n-2] - p[n])
    dp = - np.diff(phase_unwrap, 2, axis=-1)
    dp_corr = round(dp / factor)
    # need two cumsum because a change at n affects n+1 and n+2
    phase_unwrap[..., 2:] += np.cumsum(np.cumsum(dp_corr, axis=-1), axis=-1) * factor
    return phase_unwrap


def temporal_freq_est(phase_img, freq_est, te, hz=False, modify_original=False):
    # uwnrap phase_img using a frequency estimate
    # difference between phase_img and expected phase_img should be less than pi

    # method 1) add back a wrapped difference, modulo can be computed using complex exponentials
    # phase_estimate + mod(original - phase_estimate, 2*pi)

    # method 2) calculate and add the number of 2 pis required to get close to phase_estimate
    # original + round((phase_estimate - original)/(2*pi))*2*pi

    # we use method 2

    phase_img = np.array(phase_img)
    if modify_original:
        phase_unwrap = phase_img
    else:
        phase_unwrap = phase_img.copy()

    if hz:
        factor = 1
    else:
        factor = 2 * np.pi
    phase_unwrap += round((freq_est[..., np.newaxis] * te - phase_unwrap) / factor) * factor
    return phase_unwrap


def spatial_temporal(phase_img, te, weight=None, subtract_echo1=False):
    # If t1 cannot be trusted to have no wraps, methods based on temporal differences can't be used and the first
    # echo must be unwrapped using spatial methods. However, using spatial unwrapping on later echoes often fails
    # since larger TEs allow the phase difference between two spatial locations to evolve to a value larger than the
    # assumptions required by spatial unwrapping.
    # Here we unwrap the first echo using spatial methods, and then use temporal_freq_est on following echoes.
    # The frequency estimate is updated for each new echo, progressively improving the estimate.

    if subtract_echo1:
        # subtract the first echo if singularities or offsets are present
        unwrapped = np.angle(np.exp(1j * (phase_img[..., 1:] - phase_img[..., :1])))
        te = te[1:] - te[0]
    else:
        unwrapped = phase_img.copy()
    unwrapped[..., 0] = spatial_unwrap(unwrapped[..., 0])
    for echo in range(1, unwrapped.shape[-1]):
        w = weight[..., :echo] if weight is not None else None
        f = lwlsq(unwrapped[..., :echo], te[..., :echo], weight=w)

        # the following is similar to temporal_freq_est (using method 1) except we also
        # spatially unwrap the "difference from expected". This helps account for
        # susceptibility changes from breathing / motion which can cause the difference from expected
        # to be large in some areas due to varying frequency
        phase_estimate = f * te[echo]
        # mod(original - phase_estimate, 2*pi)
        unwrapped[..., echo] = np.angle(np.exp(1j * (unwrapped[..., echo] - phase_estimate)))

        unwrapped[..., echo] = (unwrapped[..., echo] + np.round((spatial_unwrap(unwrapped[..., echo]) - unwrapped[..., echo]) / (2 * np.pi)) * 2 * np.pi)

        # an entire echo volume could have a multiple 2pi offset depending on where unwrapping
        # seeds from (which is based on snr).
        # the average difference from expected should be close to 0, take away any global 2*pi offsets
        # this requires a decent mask to work
        avg = unwrapped[..., echo].mean()
        offset_int = int(avg / (2 * np.pi))
        unwrapped[..., echo] = phase_estimate + unwrapped[..., echo] - offset_int * 2 * np.pi
    return unwrapped
