import os

import setuptools

setuptools.setup(
    name="libang",
    version=os.getenv('CI_COMMIT_TAG') or '0.1',
    author="Alan Kuurstra",
    author_email="akuurstr@uwo.ca",
    description="Library for angular measurements related to fieldmaps.",
    long_description=open('README.md').read(),
    url="https://gitlab.com/Kuurstra/libang",
    packages=setuptools.find_packages(),
    install_requires=[
        'numpy',
        'scikit-image',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
)
